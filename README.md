Установка инфарструктуры на базе серверов NGINX
=========

Описание установки одного или нескольких серверов reverse proxy на базе nginx

terraform
--------------

Для начала, установите terraform
- https://learn.hashicorp.com/tutorials/terraform/install-cli

После чего нужно перейти в папку terraform и развернуть сервера при помощи команды:
    terraform init
    terraform apply --auto-approve


IP адреса
--------------

Создать файл hosts или изменить файл в каталоге /etc/ansible/hosts. В файле нужно указать ip адрес(а) вашего(их) сервера(ов):
    nginx_1 ansible_host=3.238.221.38 ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
1. nginx_1 - любое имя для сервера
2. ansible_host - ip адрес сервера на котором хотите установить nginx
3. ansible_user - пользователь под которым будет производится подключение
4. ansible_ssh_private_key - сертификат, с помощью которого возможно подключение к серверу (его нужно создать зарание)

Настройка reverse proxy
------------

Изменить в файле nginx.conf.j2 ip адрес и порт, накоторый будут перенаправлятся пакеты

Установка nginx серверов с reverse proxy
----------------

Чтобы запустить установку, выполните команду:
    ansible-playbook nginx.yml -b -vv
- -b - запуск от имени администратора
- -vv - это лог для просмотра происходящих событий

